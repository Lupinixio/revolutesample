package com.revolutesample.data.entities

import com.revolutesample.domain.entities.Currency

class CurrencyDataEntityMapper constructor() {

    fun mapToEntity(data: LatestApi): List<Currency> {

        val hashToList = data.rates.toList().sortedBy { pair: Pair<String, Double> -> pair.first }
        val result: MutableList<Currency> = arrayListOf()

        for (item in hashToList) {
            result.add(Currency(shortName = item.first, value = item.second))
        }

        return result.toList()
    }

}