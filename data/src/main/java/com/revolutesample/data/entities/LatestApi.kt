package com.revolutesample.data.entities

data class LatestApi(
    val base: String,
    val date: String,
    val rates: HashMap<String, Double>
)