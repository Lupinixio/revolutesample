package com.revolutesample.data.api

import com.revolutesample.data.entities.LatestApi
import io.reactivex.Maybe

class RevoluteApi private constructor() : IRevoluteApiClient {

    private object GetInstance {
        val INSTANCE = RevoluteApi()
    }

    companion object {
        val instance: RevoluteApi by lazy { GetInstance.INSTANCE }
    }

    override fun getLatestStockValue(productCode: String): Maybe<LatestApi> {
        return ServiceGenerator.instance.getClient(IRevoluteApiClient::class.java)
            .getLatestStockValue(productCode)
    }

}