package com.revolutesample.data.api

import com.revolutesample.data.entities.LatestApi
import io.reactivex.Flowable
import io.reactivex.Maybe
import retrofit2.http.GET
import retrofit2.http.Query

interface IRevoluteApiClient {

    @GET("/latest")
    fun getLatestStockValue(@Query("base") productCode: String): Flowable<LatestApi>
}