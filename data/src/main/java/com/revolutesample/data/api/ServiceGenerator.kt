package com.revolutesample.data.api

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ServiceGenerator private constructor() {

    private object GetInstance {
        val INSTANCE = ServiceGenerator()
    }

    companion object {
        val instance: ServiceGenerator by lazy { GetInstance.INSTANCE }
    }

    private val onHttpClient: OkHttpClient by lazy { OkHttpClient().newBuilder().build() }
    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl("https://revolut.duckdns.org")
            .client(onHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    fun <T> getClient(serviceClass: Class<T>) = retrofit.create(serviceClass)
}