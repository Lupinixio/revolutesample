package com.revolutesample.data.repository

import com.revolutesample.data.api.RevoluteApi
import com.revolutesample.domain.entities.Currency
import com.revolutesample.domain.repositories.ICurrencyRepository
import io.reactivex.Flowable

class CurrencyRepositoryImpl(private val remote: RevoluteApiImpl) : ICurrencyRepository {
    override fun getCurrencyConvertingValue(baseCurrency: String): Flowable<List<Currency>> {
        return remote.getCurrencyConversionFactor(baseCurrency)
    }

}