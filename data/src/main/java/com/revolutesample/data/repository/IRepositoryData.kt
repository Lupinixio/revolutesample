package com.revolutesample.data.repository

import com.revolutesample.domain.entities.Currency
import io.reactivex.Flowable

interface IRepositoryData {
    fun getCurrencyConversionFactor(baseCurrency: String): Flowable<List<Currency>>
}