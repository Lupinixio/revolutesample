package com.revolutesample.data.repository

import com.revolutesample.data.api.IRevoluteApiClient
import com.revolutesample.data.entities.CurrencyDataEntityMapper
import com.revolutesample.domain.entities.Currency
import io.reactivex.Flowable

class RevoluteApiImpl constructor(private val api: IRevoluteApiClient) : IRepositoryData {

    private val mapper = CurrencyDataEntityMapper()

    override fun getCurrencyConversionFactor(baseCurrency: String): Flowable<List<Currency>> {
        return api.getLatestStockValue(baseCurrency).map { mapper.mapToEntity(it) }
    }

}