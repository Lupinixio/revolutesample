package com.revolutesample.domain.entities

data class Currency(
    val shortName: String,
    var longName: String = "",
    var image: Int = 0,
    val value: Double
)