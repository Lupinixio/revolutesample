package com.revolutesample.domain

import com.revolutesample.domain.common.BaseFlowableUseCase
import com.revolutesample.domain.common.FlowableRxTransformer
import com.revolutesample.domain.entities.Currency
import com.revolutesample.domain.repositories.ICurrencyRepository
import io.reactivex.Flowable
import io.reactivex.subjects.PublishSubject

class CurrencyConverter(
    private val transformer: FlowableRxTransformer<List<Currency>>,
    private val repository: ICurrencyRepository
) : BaseFlowableUseCase<List<Currency>>(transformer) {


    override fun createFlowable(data: Map<String, Any>?): Flowable<List<Currency>> {
        return repository.getCurrencyConvertingValue("EUR")
    }

    private val DEFAULT_CURRENCY = "EUR"

    val currencyConvertedList: PublishSubject<List<Currency>> = PublishSubject.create()

    fun getConvertedValueForCurrency(currency: String): Flowable<List<Currency>> {
        return repository.getCurrencyConvertingValue(currency)
    }
}