package com.revolutesample.domain.repositories

import com.revolutesample.domain.entities.Currency
import io.reactivex.Flowable
import io.reactivex.Maybe

interface ICurrencyRepository {
    fun getCurrencyConvertingValue(baseCurrency: String): Flowable<List<Currency>>
}