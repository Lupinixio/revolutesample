package com.revolutsample.adapter

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.revolutsample.BR
import com.revolutsample.base.ItemClickListener

abstract class BaseViewHolder<T, B : ViewDataBinding>(
    val binding: B,
    private val clickListener: ItemClickListener<T>? = null
) :
    RecyclerView.ViewHolder(binding.root) {

    open fun bind(obj: T) {
        itemView.setOnClickListener { clickListener?.onItemClick(obj) }
        binding.setVariable(BR.obj, obj)
        binding.executePendingBindings()

    }
}