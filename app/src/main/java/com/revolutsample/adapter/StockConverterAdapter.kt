package com.revolutsample.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.revolutsample.R
import com.revolutsample.base.ItemClickListener
import com.revolutsample.databinding.ItemStockConverterBinding
import com.revolutsample.model.CurrencyModel
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.item_stock_converter.view.*

class StockConverterAdapter(
    private val list: MutableList<CurrencyModel>,
    private val itemClickListener: ItemClickListener<CurrencyModel>
) :
    RecyclerView.Adapter<CurrencyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val v =
            DataBindingUtil.inflate<ItemStockConverterBinding>(
                LayoutInflater.from(parent.context),
                R.layout.item_stock_converter,
                parent,
                false
            )
        return CurrencyViewHolder(v, itemClickListener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        val current = list[position]
        holder.bind(current)

    }

    fun setItems(list: MutableList<CurrencyModel>) {
        this.list.apply {
            clear()
            addAll(list)
        }
        notifyDataSetChanged()
    }


}

class CurrencyViewHolder(binding: ItemStockConverterBinding, clickListener: ItemClickListener<CurrencyModel>) :
    BaseViewHolder<CurrencyModel, ItemStockConverterBinding>(binding, clickListener)