package com.revolutsample.base

interface ItemClickListener<T> {
    fun onItemClick(itemClicked: T)
}