package com.revolutsample.base

import android.text.InputFilter
import android.text.Spanned
import android.widget.EditText
import java.util.regex.Pattern

class DecimalDigitsInputFilter : InputFilter {

    private var mPattern: Pattern = Pattern.compile("[0-9]+((\\.[0-9]{0,2})?)||(\\.)?")

    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dstart: Int,
        dend: Int
    ): CharSequence? {

        val replacement = source.subSequence(start, end).toString()
        val newValue =
            dest.subSequence(0, dstart).toString() + replacement + dest.subSequence(dend, dest.length).toString()

        val matcher = mPattern.matcher(newValue)
        return when {
            matcher.matches() -> return null
            source.isEmpty() -> dest.subSequence(dstart, dend)
            else -> ""
        }
    }

}

fun EditText.setDecimalFilter() {
    this.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter())
}