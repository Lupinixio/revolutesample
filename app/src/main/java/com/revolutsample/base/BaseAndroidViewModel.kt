package com.revolutsample.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseAndroidViewModel(application: Application) : AndroidViewModel(application) {

    val TAG = javaClass.simpleName

    val error: MutableLiveData<Throwable> = MutableLiveData()
    val loadingInProgress: MutableLiveData<Boolean> = MutableLiveData()
    val navDirection = MutableLiveData<NavDirections>()
    val toastMessage = MutableLiveData<String>()

    private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }

    abstract fun setupMediator()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    protected fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    protected fun clearAllDisposable() {
        compositeDisposable.clear()
    }
}