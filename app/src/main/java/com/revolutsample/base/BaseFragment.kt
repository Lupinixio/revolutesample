package com.revolutsample.base

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController

abstract class BaseFragment<VM : BaseAndroidViewModel, B : ViewDataBinding> : Fragment() {

    private val navController: NavController by lazy { findNavController() }
    private var toast: Toast? = null
    private var activityStatus: IActivityStatus? = null

    val TAG = javaClass.simpleName

    protected lateinit var viewModel: VM
    protected lateinit var binding: B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        activityStatus = context as IActivityStatus
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewModelObservers()
        setupInitialView()
    }

    protected open fun initViewModel() {
        viewModel = ViewModelProviders.of(this).get(getViewModelClass())
        viewModel.setupMediator()
        viewModel.error.observe(this, onErrorChange())
        viewModel.loadingInProgress.observe(this, onLoadingProgressChange())
        viewModel.navDirection.observe(this, onNavigationDirectionChange())
        viewModel.toastMessage.observe(this, onToastMessageChange())
    }

    protected abstract fun getViewModelClass(): Class<VM>

    protected abstract fun setupInitialView()

    protected abstract fun setupViewModelObservers()

    protected open fun onLoadingProgressChange(): Observer<in Boolean> {
        return Observer { activityStatus?.isLoading(it) }
    }

    protected open fun onErrorChange(): Observer<in Throwable> {
        return Observer { showToastMessage(it.localizedMessage) }
    }

    protected open fun onToastMessageChange(): Observer<in String> {
        return Observer { showToastMessage(it) }
    }

    protected open fun onNavigationDirectionChange(): Observer<in NavDirections> {
        return Observer { navController.navigate(it) }
    }

    protected fun showToastMessage(message: String, duration: Int = Toast.LENGTH_LONG) {
        this.toast?.let {
            it.setText(message)
            it.duration = duration
            it.show()
        } ?: run {
            toast = Toast.makeText(context, message, duration)
            toast!!.show()
        }

    }


}