package com.revolutsample.base

interface IActivityStatus {
    fun isLoading(isLoading: Boolean)
}