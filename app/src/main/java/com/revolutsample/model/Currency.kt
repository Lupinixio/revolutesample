package com.revolutsample.model

data class Currency(val id: String, val exchangeRate: Double)