package com.revolutsample.model

import android.graphics.drawable.Drawable
import android.util.Log
import androidx.databinding.*
import com.revolutsample.BR
import io.reactivex.Observer
import io.reactivex.SingleObserver
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

class CurrencyModel(
    val id: String,
    val country: ObservableField<String> = ObservableField(""),
    val flagImage: ObservableInt = ObservableInt(0)
) {

    var amountToConvert: Double = 0.00
    var exchangeRate: Double = 1.0000
    var convertedAmount: Double = 0.00

    private val compositeDisposable by lazy { CompositeDisposable() }

    val formattedAmount = ObservableField("0.00")

    fun getAmountSubscriber(): Observer<Double> {
        return object : Observer<Double> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
                compositeDisposable.add(d)
            }

            override fun onNext(t: Double) {
                amountToConvert = t
                updateFormattedAmount(executeConversion(amountToConvert, exchangeRate))
            }

            override fun onError(e: Throwable) {

            }

        }
    }

    fun getExchangeRateSubscriber(): Observer<Map.Entry<String, Double>> {
        return object : Observer<Map.Entry<String, Double>> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
                compositeDisposable.add(d)
            }

            override fun onNext(t: Map.Entry<String, Double>) {
                Log.d("CurrencyModel", "exchangeRateEmission -> currency $id")
                exchangeRate = t.value
                updateFormattedAmount(executeConversion(amountToConvert, exchangeRate))
            }

            override fun onError(e: Throwable) {

            }

        }
    }

    fun getFlagImageSubscriber(): SingleObserver<Int> {
        return object : SingleObserver<Int> {
            override fun onSuccess(t: Int) {
                flagImage.set(t)
            }

            override fun onSubscribe(d: Disposable) {
                compositeDisposable.add(d)
            }

            override fun onError(e: Throwable) {

            }

        }
    }

    fun getCountryNameSubscriber(): SingleObserver<String> {
        return object : SingleObserver<String> {
            override fun onSuccess(t: String) {
                country.set(t)
            }

            override fun onSubscribe(d: Disposable) {
                compositeDisposable.add(d)
            }

            override fun onError(e: Throwable) {

            }

        }
    }

    fun unsubscribeObserver() {
        compositeDisposable.dispose()
    }

    private fun updateFormattedAmount(value: Double) {
        val amount = String.format("%.2f", value)
        convertedAmount = value
        formattedAmount.set(amount)
    }

    private fun executeConversion(amountToConvert: Double, exchangeRate: Double) = amountToConvert * exchangeRate

}