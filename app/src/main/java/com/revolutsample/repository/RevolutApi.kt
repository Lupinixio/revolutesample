package com.revolutsample.repository

import com.revolutsample.repository.api.LatestApi
import io.reactivex.Flowable

class RevolutApi private constructor() : IRevolutApiClient {

    private object GetInstance {
        val INSTANCE = RevolutApi()
    }

    companion object {
        val instance: RevolutApi by lazy { GetInstance.INSTANCE }
    }

    override fun getLatestStockValue(productCode: String): Flowable<LatestApi> {
        return ServiceGenerator.instance.getClient(IRevolutApiClient::class.java)
            .getLatestStockValue(productCode)
    }

}