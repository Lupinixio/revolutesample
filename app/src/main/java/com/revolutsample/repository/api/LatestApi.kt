package com.revolutsample.repository.api

data class LatestApi(
    val base: String,
    val date: String,
    val rates: HashMap<String, Double>
)