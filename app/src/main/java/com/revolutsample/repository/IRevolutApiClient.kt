package com.revolutsample.repository

import com.revolutsample.repository.api.LatestApi
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

interface IRevolutApiClient {

    @GET("/latest")
    fun getLatestStockValue(@Query("base") productCode: String): Flowable<LatestApi>
}