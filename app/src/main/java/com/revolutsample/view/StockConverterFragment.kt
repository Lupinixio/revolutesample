package com.revolutsample.view

import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.revolutsample.R
import com.revolutsample.adapter.StockConverterAdapter
import com.revolutsample.base.BaseFragment
import com.revolutsample.base.DecimalDigitsInputFilter
import com.revolutsample.base.ItemClickListener
import com.revolutsample.base.setDecimalFilter
import com.revolutsample.databinding.FragmentStockConverterBinding
import com.revolutsample.model.CurrencyModel
import com.revolutsample.view.viewmodel.StockConverterViewModel

class StockConverterFragment : BaseFragment<StockConverterViewModel, FragmentStockConverterBinding>(),
    ItemClickListener<CurrencyModel> {

    private val currencyAdapter = StockConverterAdapter(mutableListOf(), this)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_stock_converter, container, false)
        binding.lifecycleOwner = this
        binding.baseCurrency = viewModel.getSelectedBaseCurrency()


        return binding.root
    }

    override fun onResume() {
        super.onResume()
        viewModel.resumeGettingData()
    }

    override fun onPause() {
        super.onPause()
        viewModel.pauseGettingData()
    }

    override fun getViewModelClass(): Class<StockConverterViewModel> {
        return StockConverterViewModel::class.java
    }

    override fun setupInitialView() {

        binding.stockRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = currencyAdapter
        }

        binding.amountInputField.setDecimalFilter()

        binding.amountInputField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                viewModel.setAmountToConvert(p0.toString())
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
    }

    override fun setupViewModelObservers() {
        viewModel.getCurrencyList().observe(this, onCurrencyListChange())
        viewModel.getSelectedBaseCurrency().observe(this, onSelectedBaseCurrencyChange())
    }

    override fun onItemClick(itemClicked: CurrencyModel) {
        viewModel.setSelectedBaseCurrency(itemClicked)
    }

    private fun onCurrencyListChange(): Observer<List<CurrencyModel>> {
        return Observer { currencyAdapter.setItems(it.toMutableList()) }
    }

    private fun onSelectedBaseCurrencyChange(): Observer<in CurrencyModel> {
        return Observer { binding.amountInputField.setText(String.format("%.2f", it.amountToConvert)) }
    }


}