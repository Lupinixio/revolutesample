package com.revolutsample.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.revolutsample.R
import com.revolutsample.base.IActivityStatus
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), IActivityStatus {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun isLoading(isLoading: Boolean) {
        progressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
    }
}
