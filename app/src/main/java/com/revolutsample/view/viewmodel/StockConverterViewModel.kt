package com.revolutsample.view.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.revolutsample.R
import com.revolutsample.base.BaseAndroidViewModel
import com.revolutsample.domain.CurrencyConverter
import com.revolutsample.model.Currency
import com.revolutsample.model.CurrencyModel
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject

class StockConverterViewModel(application: Application) : BaseAndroidViewModel(application) {

    private val initialBaseCurrency = "EUR"
    private val initialAmount = 100.00
    private val initialCurrencyModel = CurrencyModel(id = initialBaseCurrency).apply { amountToConvert = initialAmount }
    private val currencyConverter by lazy { CurrencyConverter() }
    private var exchangeRateDisposable: Disposable? = null
    private var previousSelectedBase: String = ""
    private val selectedBase = MutableLiveData<CurrencyModel>()
    private val currencyList = MutableLiveData<List<CurrencyModel>>().apply { value = emptyList() }
    private var isPaused = false
    private val amountToConvertSubject: PublishSubject<Double> = PublishSubject.create()
    private val newExchangeRateSubject: PublishSubject<Map.Entry<String, Double>> = PublishSubject.create()

    init {
        selectedBase.value = initialCurrencyModel
    }

    override fun setupMediator() {
        selectedBase.observeForever(onSelectedCurrencyChange())
        currencyList.observeForever(onCurrencyListChange())
    }


    fun getSelectedBaseCurrency() = selectedBase

    fun setSelectedBaseCurrency(currency: CurrencyModel) {
        previousSelectedBase = selectedBase.value?.id ?: ""
        selectedBase.value = CurrencyModel(currency.id).apply {
            amountToConvert = currency.convertedAmount
        }

    }

    fun getCurrencyList() = currencyList

    fun setAmountToConvert(amountToConvert: String) {
        when {
            amountToConvert.isNotEmpty() -> updateAmountToConvert(amountToConvert.toDouble())
            else -> updateAmountToConvert(0.00)
        }
    }

    fun pauseGettingData() {
        isPaused = true
        exchangeRateDisposable?.dispose()
    }

    fun resumeGettingData() {
        if (isPaused && selectedBase.value != null) {
            getExchangeRateContinuously(selectedBase.value!!)
        }
    }

    private fun updateAmountToConvert(amountToConvert: Double) {
        amountToConvertSubject.onNext(amountToConvert)
    }

    private fun onSelectedCurrencyChange(): Observer<in CurrencyModel> {
        return Observer {
            getCountryFlagById(it.id).subscribe(it.getFlagImageSubscriber())
            getCountryNameById(it.id).subscribe(it.getCountryNameSubscriber())
            currencyConverter.getCurrencyList(it)
                .flatMapObservable { list -> Observable.fromIterable(list) }
                .map { currency ->
                    CurrencyModel(currency.id).apply {
                        exchangeRate = currency.exchangeRate
                        amountToConvertSubject.subscribe(getAmountSubscriber())
                        newExchangeRateSubject
                            .filter { value -> value.key == currency.id }
                            .subscribeOn(Schedulers.computation())
                            .subscribe(getExchangeRateSubscriber())
                        getCountryNameById(currency.id).subscribe(getCountryNameSubscriber())
                        getCountryFlagById(currency.id).subscribe(getFlagImageSubscriber())
                    }
                }.toList().map { list -> movePreviousBaseCurrencyToTop(list.toMutableList(), previousSelectedBase) }
                .subscribe(onGetNewCurrencyList())
        }
    }

    private fun movePreviousBaseCurrencyToTop(
        list: MutableList<CurrencyModel>,
        previousValue: String
    ): List<CurrencyModel> {
        var elementToMove: CurrencyModel? = null
        loop@ for (item in list) {
            if (item.id == previousValue) {
                elementToMove = item
                list.remove(item)
                break@loop
            }
        }
        if (elementToMove != null) {
            list.add(0, elementToMove)
        }

        return list
    }

    private fun onCurrencyListChange(): Observer<List<CurrencyModel>> {
        return Observer {
            getExchangeRateContinuously(selectedBase.value ?: initialCurrencyModel)
        }
    }

    private fun getExchangeRateContinuously(baseCurrency: CurrencyModel) {
        exchangeRateDisposable?.dispose()
        val disposable = currencyConverter.getUpdatedExchangeRate(baseCurrency)
            .map { remapList(it) }
            .flatMap { Flowable.fromIterable(it.asIterable()) }
            .subscribe({
                newExchangeRateSubject.onNext(it)
            }, { e -> error.postValue(e) })


        exchangeRateDisposable = disposable
        addDisposable(disposable)
    }

    private fun remapList(list: List<Currency>) = list.map { it.id to it.exchangeRate }.toMap()


    private fun onGetNewCurrencyList(): SingleObserver<List<CurrencyModel>> {

        currencyList.value?.forEach {
            it.unsubscribeObserver()
        }

        return object : SingleObserver<List<CurrencyModel>> {
            override fun onSuccess(t: List<CurrencyModel>) {
                currencyList.postValue(t)
                amountToConvertSubject.onNext(selectedBase.value?.amountToConvert ?: initialAmount)
                loadingInProgress.postValue(false)
            }

            override fun onSubscribe(d: Disposable) {
                addDisposable(d)
                loadingInProgress.postValue(true)
            }

            override fun onError(e: Throwable) {
                error.postValue(e)
                loadingInProgress.postValue(false)
            }

        }
    }

    private fun getCountryNameById(id: String): Single<String> {
        return Single.just(id)
            .map { countryName[it] ?: "" }
            .subscribeOn(Schedulers.io())
    }

    private fun getCountryFlagById(id: String): Single<Int> {
        return Single.just(id)
            .map { countryFlag[id] ?: 0 }
            .subscribeOn(Schedulers.io())
    }

    private val countryName = hashMapOf(
        "AUD" to application.getString(R.string.australia),
        "BGN" to application.getString(R.string.bulgaria),
        "BRL" to application.getString(R.string.brazil),
        "CAD" to application.getString(R.string.canada),
        "CHF" to application.getString(R.string.switzerland),
        "CNY" to application.getString(R.string.china),
        "CZK" to application.getString(R.string.czech_republic),
        "DKK" to application.getString(R.string.denmark),
        "GBP" to application.getString(R.string.united_kingdom),
        "HKD" to application.getString(R.string.hong_kong),
        "HRK" to application.getString(R.string.croatia),
        "HUF" to application.getString(R.string.hungary),
        "IDR" to application.getString(R.string.indonesia),
        "ILS" to application.getString(R.string.israel),
        "INR" to application.getString(R.string.india),
        "ISK" to application.getString(R.string.iceland),
        "JPY" to application.getString(R.string.japan),
        "KRW" to application.getString(R.string.south_korea),
        "MXN" to application.getString(R.string.mexico),
        "MYR" to application.getString(R.string.malaysia),
        "NOK" to application.getString(R.string.norway),
        "NZD" to application.getString(R.string.new_zeland),
        "PHP" to application.getString(R.string.philippines),
        "PLN" to application.getString(R.string.poland),
        "RON" to application.getString(R.string.romania),
        "RUB" to application.getString(R.string.russia),
        "SEK" to application.getString(R.string.sweden),
        "SGD" to application.getString(R.string.singapore),
        "THB" to application.getString(R.string.thailand),
        "TRY" to application.getString(R.string.turkey),
        "USD" to application.getString(R.string.united_states),
        "ZAR" to application.getString(R.string.south_africa),
        "EUR" to application.getString(R.string.european_union)
    )

    private val countryFlag = hashMapOf(
        "AUD" to R.drawable.ic_australia,
        "BGN" to R.drawable.ic_bulgaria,
        "BRL" to R.drawable.ic_brazil,
        "CAD" to R.drawable.ic_canada,
        "CHF" to R.drawable.ic_switzerland,
        "CNY" to R.drawable.ic_china,
        "CZK" to R.drawable.ic_czech_republic,
        "DKK" to R.drawable.ic_denmark,
        "GBP" to R.drawable.ic_united_kingdom,
        "HKD" to R.drawable.ic_hong_kong,
        "HRK" to R.drawable.ic_croatia,
        "HUF" to R.drawable.ic_hungary,
        "IDR" to R.drawable.ic_indonesia,
        "ILS" to R.drawable.ic_israel,
        "INR" to R.drawable.ic_india,
        "ISK" to R.drawable.ic_iceland,
        "JPY" to R.drawable.ic_japan,
        "KRW" to R.drawable.ic_south_korea,
        "MXN" to R.drawable.ic_mexico,
        "MYR" to R.drawable.ic_malaysia,
        "NOK" to R.drawable.ic_norway,
        "NZD" to R.drawable.ic_new_zeland,
        "PHP" to R.drawable.ic_philippines,
        "PLN" to R.drawable.ic_poland,
        "RON" to R.drawable.ic_romania,
        "RUB" to R.drawable.ic_russia,
        "SEK" to R.drawable.ic_sweden,
        "SGD" to R.drawable.ic_singapore,
        "THB" to R.drawable.ic_thailand,
        "TRY" to R.drawable.ic_turkey,
        "USD" to R.drawable.ic_united_states,
        "ZAR" to R.drawable.ic_south_africa,
        "EUR" to R.drawable.ic_european_union
    )

}