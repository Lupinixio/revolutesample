package com.revolutsample.domain

import com.revolutsample.model.Currency
import com.revolutsample.model.CurrencyModel
import com.revolutsample.repository.RevolutApi
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class CurrencyConverter {

    val UPDATE_RATE_TIME_IN_SECOND = 1L

    fun getCurrencyList(baseCurrency: CurrencyModel): Single<List<Currency>> {
        return RevolutApi.instance.getLatestStockValue(baseCurrency.id)
            .retry()
            .flatMap { Flowable.fromIterable(it.rates.asIterable()) }
            .map { Currency(it.key, it.value) }
            .sorted { currency1, currency2 -> currency1.id.compareTo(currency2.id) }
            .toList().subscribeOn(Schedulers.io())
    }

    fun getUpdatedExchangeRate(
        baseCurrency: CurrencyModel,
        updateRate: Long = UPDATE_RATE_TIME_IN_SECOND
    ): Flowable<List<Currency>> {
        return Flowable.interval(updateRate, TimeUnit.SECONDS)
            .flatMap { getCurrencyList(baseCurrency).toFlowable().onBackpressureLatest() }
    }
}

